# How to de execução do ambiente Vagrant + Ansible + Docker + Go

*Este procedimento assume que já tenha o Vagrant instalado:
Vagrant 2.2.3

## Execução do Vagrant para criação dos ambientes VBox vm_docker e ansible

### Iniciar o box vm_docker
1) Acessar o diretório vm_docker:

cd vm_docker

2) Iniciar o box do vm_docker:

vagrant up

### Iniciar o box ansible
1) Acessar o diretório ansible :

cd ansible 

2) Iniciar o box do ansible:

vagrant up

### Build da aplicação app.go e imagem do Docker

1) Acessar o diretóio ansible:

cd ansible

2) Acessar o box ansible:

vagrant ssh

3) Acessar o diretório com os Playbooks do Ansible:

cd /vagrant

4) Inicar o build do Còdigo app.go e da imagem do Docker:

ansible-playbook buildImageGoApp_PB.yml


5) Execução do Container do app:

ansible-playbook runGoAppContainer_PB.yml


### Verificar o build e execução do container
1) Acessar o diretório vm_docer:

cd vm_docker

2) Acessar o box vm_docker:

vagrant ssh

3) Verificar a existencia da Imagem "goapp" :

docker images

4) Verificar a existencia do container em execução do "goapp":

docker ps

### Teste do "app"
1) Clicar nos links abaixo:

http://192.168.10.100/app

https://192.168.10.100/app