package main

import (
    "log"
    "net/http"
)



//Funcao para execucao do http.Listen em paralelo para http e https
func ListenApp(addr string, sslAddr string, ssl map[string]string) chan error {

    errs := make(chan error)

    go func() {

        if err := http.ListenAndServe(addr, nil); err != nil {
            errs <- err
        }

    }()

    go func() {
        if err := http.ListenAndServeTLS(sslAddr, ssl["cert"], ssl["key"], nil); err != nil {
            errs <- err
        }
    }()

    return errs
}

//Funcao de String de response
func ResponseApp(w http.ResponseWriter, req *http.Request) {
    w.Header().Set("Content-Type", "text/plain")
    w.Write([]byte("App Teste!\n"))
}



func main() {

    // Handle de contexto /app para chamado do response
    http.HandleFunc("/app", ResponseApp)

    // Chamada do Funcao ListenApp em paralelo portas 80 e 443 como parametros
    errs := ListenApp(":80", ":443", map[string]string{
        "cert": "server.crt",
        "key":  "server.key",
    })

    select {
    case err := <-errs:
        log.Printf("Erro (error: %s)", err)
    }

}
